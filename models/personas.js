const { guardarDB } = require("../helpers/guardarArchivo");

class Personas {

  constructor() {
    this._listado = [];
  }

  crearPersona(persona = {}) {
    this._listado.push(persona);
  }

  get listadoArr(){
    const listado = this._listado;
    //Me devuelve un arreglo de todas las key
    Object.keys(this._listado).forEach(key =>{
        console.log(key);
        const persona = this._listado[key];
        listado.push(persona);
    });

    console.log(listado);
    return listado;
}

  cargarPersonasArray(personas= []){
   personas.forEach(persona =>{
    this._listado[persona.id]= persona;
  })
} 

  cambiarPersona(id) {
    console.log(this._listado);
    
    delete this._listado[id];
    console.log(this._listado);
  }

}

module.exports = Personas;