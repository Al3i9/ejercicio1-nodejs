const { guardarDB, leerDB } = require("../helpers/guardarArchivo");
const Persona = require("../models/persona");
const Personas = require("../models/personas");

const { response } = 'express';

const personas = new Personas();

 


const personaGet = (req, res = response) => {

  const personasDB = leerDB();
  if(personasDB){
  personas.cargarPersonasArray(personasDB)
  }

  res.json({
    msg: 'get API - Controlador',
    personasDB
  });
};



const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;

  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
  const personasDB = leerDB();
  if(personasDB){
    personas.cargarPersonasArray(personasDB)
  }
   personas.crearPersona(persona);
  guardarDB(personas.listadoArr);
  personasDB = leerDB();
   
   res.json({
    msg: 'post API - Controlador',
    personasDB
  });


};

const personaPut = (req, res = response) => {

  const personasDB = leerDB();
  if(personasDB){
    personas.cargarPersonasArray(personasDB)
  }

  const { id } = req.params;

  if(id){
    personas.cambiarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci, direccion, sexo);
    persona.setID(id);
    personas.crearPersona(persona);
    guardarDB(personas.listadoArr);
  }  

  res.json({
    msg: 'put API - Controlador'
  });
};


const personaDelete = (req, res = response) => {

  const personasDB = leerDB();
  if(personasDB){
    personas.cargarPersonasArray(personasDB)
  }

  const {id} = req.params;
  
  if (id) {
    personas.cambiarPersona(id);
    guardarDB(personas.listadoArr);
  }


  res.json({
    msg: 'delete API - Controlador'
  });
};


module.exports = {
  personaGet,
  personaPut,
  personaPost,
  personaDelete
}